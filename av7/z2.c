#include <stdio.h>
#define MAX 100
int main() {
	int a[MAX][MAX], M, N, i, j;
	int suma = 0;
	printf("Vnesete M i N: \n");
	scanf("%d %d", &M, &N);
	printf("Vnesete ja matricata: \n");
	for (i = 0; i < M; i++) {
		for (j = 0; j < N; j++) {
			printf("a[%d][%d] = ", i, j);
			scanf("%d", &a[i][j]);
		}
	}
	for (i = 0; i < M; i++) {
		for (j = 0; j < N; j++) {
			int sh = 0;
			int sv = 0;
			if(j > 0) sh += a[i][j - 1];
			if(j < N - 1) sh += a[i][j + 1];
			if(i > 0) sv += a[i - 1][j];
			if(i < M - 1) sv += a[i + 1][j];
            if(sh > sv) suma += a[i][j];
		}
	}
	printf("Sumata e: %d\n", suma);
	return 0;
}

