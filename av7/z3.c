#include <stdio.h>
#define MAX 100
int main() {
	int a[MAX][MAX], M, N, i, j;
	printf("Vnesete M i N: \n");
	scanf("%d %d", &M, &N);
	printf("Vnesete ja matricata: \n");
	for (i = 0; i < M; i++) {
		for (j = 0; j < N; j++) {
			printf("a[%d][%d] = ", i, j);
			scanf("%d", &a[i][j]);
		}
	}
	for (i = 0; i < M / 2; i++) {
		int temp = a[i][i];
		a[i][i] = a[M - 1 - i][M - 1 - i];
		a[M - 1 - i][M - 1 - i] = temp;
		temp = a[i][M - 1 - i];
		a[i][M - 1 - i] = a[M - 1 - i][i];
		a[M - 1 - i][i] = temp;
	}
	printf("Rezultantnata matrica e: \n");
	for (i = 0; i < M; i++) {
		for (j = 0; j < N; j++) {
			printf("%d\t", a[i][j]);
		}
		printf("\n");
	}
	return 0;
}

