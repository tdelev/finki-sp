#include <stdio.h>
#define MAX 100
int main() {
	int a[MAX][MAX], M, N, i, j;
	int suma[MAX];
	printf("Vnesete M i N: \n");
	scanf("%d %d", &M, &N);
	printf("Vnesete ja matricata: \n");
	for (i = 0; i < M; i++) {
		suma[i] = 0;
		for (j = 0; j < N; j++) {
			printf("a[%d][%d] = ", i, j);
			scanf("%d", &a[i][j]);
		}
	}
	for (i = 0; i < M; i++) {
		for (j = 0; j < N; j++) {
			suma[i] += a[i][j];
		}
	}
	for (i = 0; i < M; i++) {
		for (j = 0; j < N; j++) {
			a[i][j] -= suma[i] * 1.0 / N;
		}
	}

	printf("Rezultantnata matrica e: \n");
	for (i = 0; i < M; i++) {
		for (j = 0; j < N; j++) {
			printf("%d\t", a[i][j]);
		}
		printf("\n");
	}
	return 0;
}

