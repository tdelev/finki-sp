#include <stdio.h>
#include <stdlib.h>
#define MAX 80

int main(int argc, char* argv[]) {
	char s[MAX], *c;
	int vozbor = 0, brZborovi = 0;
	FILE *vlezna, *izlezna;
	if (argc == 2) {
		printf("Upotreba: %s ime_na_vlezna_datoteka ime_na_vlezna_datoteka\n",
				argv[0]);
		exit(-1);
	}
	if ((vlezna = fopen(argv[1], "r")) == NULL) {
		printf("Datotekata %s ne se otvora.\n", argv[1]);
		exit(-1);
	}
	if ((izlezna = fopen(argv[2], "w")) == NULL) {
		printf("Datotekata %s ne se otvora\n", argv[2]);
		exit(-1);
	}
	while ((fgets(s, MAX, vlezna)) != NULL) {
		c = s;
		vozbor = 0;
		brZborovi = 0;
		while (*c != '\0') {
			if (isalnum(*c)) {
				if (!vozbor)
					vozbor = 1;
			} else if (vozbor) {
				vozbor = 0;
				brZborovi++;
			}
			c++;
		}
		if (vozbor)
			brZborovi++;
		fprintf(izlezna, "%d %s", brZborovi, s);
	}
	fclose(vlezna);
	fclose(izlezna);
	return 0;
}

