#include <stdio.h>
float sum(int n) {
    if(n == 1) return 1;
    return 1.0 / n + sum(n - 1);
}
int main () {
    int n;
    printf("Vnesi broj:\n");
    scanf("%d", &n);
    printf("sum(%d) = %.2f\n", n, sum(n));
    return 0;
}

