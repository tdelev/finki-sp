#include <stdio.h>
int eprost(int n);
int zbircif(int n);
int main () {
    int br = 0, i;
    for (i = 2; i <= 9999; i++) {
        if (eprost(i) && eprost(zbircif(i))) {
            printf("Brojot %d go zadovoluva uslovot\n",i);
            br++;
        }
    }
    printf("Pronajdeni se %d broevi koi go zadovoluvaat uslovot\n",br);
    return 0;
}
int eprost(int n) {
    int i, prost;
    if (n < 4) prost = 1;
    else
    if ((n % 2) == 0) prost = 0;
    else {
        i = 3; prost = 1;
        while ((i * i <= n) && prost) {
            if (n % i == 0) prost = 0;
            i += 2;
        }
    }
    return prost;
}
int zbircif(int n) {
    int zbir = 0;
    while (n > 0) {
        zbir += (n % 10);
        n /= 10;
    }
    return zbir;
}

