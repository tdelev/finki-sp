#include <stdio.h>
int fibonaci(int n) {
    if(n == 0 || n == 1) return 1;
    return fibonaci(n - 1) + fibonaci(n - 2);
}
int main () {
    int n;
    printf("Vnesi broj:\n");
    scanf("%d", &n);
    printf("fib(%d) = %d\n", n, fibonaci(n));
    return 0;
}

