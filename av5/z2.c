#include <stdio.h>
int zb2cif(int n) {
    int zbir;
    zbir=(n%100)+(n/100);
    return zbir;
}
int main() {
    int br = 0, i;
    for (i = 1000; i <= 9999; i++) {
        if (i%zb2cif(i) == 0) {
            printf("Brojot %d go zadovoluva uslovot\n", i);
            br++;
        }
    }
    printf("Pronajdeni se %d broevi koi go zadovoluvaat uslovot\n", br);
    return 0;
}

