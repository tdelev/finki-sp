#include <stdio.h>
/* Deklaracija na funkcii */
void printMax(int broj);
void printPozdrav();
int main() {
    int k = 10;
    printPozdrav(); /* Pecati Pozdrav */
    printMax(k); /* Ja pecati maximalnata vrednost */
    return 0;
}
/* Definicija na funkciite */
void printMax(int broj) {
    printf("Maksimalniot broj e %d\n",broj);
}
void printPozdrav() {
    printf("Dobar Den. Kako se cuvstvuvate denes?\n");
}

