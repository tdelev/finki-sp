#include <stdio.h>
int prost(int n) {
    int k = 2;
    while (k * k <= n) {
        if (n % k==0) return 0;
        k++;
    }
    return 1;
}
int prostgore(int n) {
    do
        n++;
    while (!(prost(n)));
    return n;
}
int main() {
    int broj, razlika;
    printf("Vnesi broj\n");
    scanf("%d", &broj);
    razlika = prostgore(broj)-broj;
    printf("Razlikata medu prostiot broj %d i %d e %d\n", prostgore(broj), broj, razlika);
    return 0;
}

